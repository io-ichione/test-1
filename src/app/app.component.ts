import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public todoform = {};
  public listTodo: any[] = [];
  public api = 'https://jsonplaceholder.typicode.com/';

  constructor(private fb: FormBuilder) {
    // Tambahkan validasi untuk field 'title' dengan validasi 'Required'
    this.todoform = this.fb.group({
      id: null,
      userId: '',
      title: '',
      body: ''
    });
  }

  public ngOnInit() {
  }

  public onSubmit() {
    // Gunakan http.POST (Untuk Tambah Baru) & http.PUT (Untuk Ubah Data) dari Restful API yang didefinisikan pada variable api
  }

  public onLoad() {
    // Gunakan http.GET (Untuk mengambil data untuk table)
  }

  public onRemove() {
    // Gunakan http.DELETE (Untuk menghapus data untuk table)
  }
}
